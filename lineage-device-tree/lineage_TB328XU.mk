#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TB328XU device
$(call inherit-product, device/lenovo/TB328XU/device.mk)

PRODUCT_DEVICE := TB328XU
PRODUCT_NAME := lineage_TB328XU
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := TB328XU
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-unisoc

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="ums512_1h10_Natv_Tablet-user 12 SP1A.210812.016 TB328XU_S200144_230308_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/TB328XU/TB328XU:12/SP1A.210812.016/TB328XU_S200144_230308_ROW:user/release-keys
